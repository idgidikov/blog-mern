import { InferSchemaType,model, Schema } from "mongoose";

const postSchema = new Schema(
    {
    userId: { type: Schema.Types.ObjectId, required: true },
    title : {type: String, required: true },
    text: {type: String,  },
    
},{timestamps: true});

type Post = InferSchemaType<typeof postSchema>

export default model<Post>("Post",postSchema);
