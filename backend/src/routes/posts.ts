import * as PostsController from "../controllers/posts";
import express from "express";
import { requiresAuth } from "../middleware/auth";
const router = express.Router();

router.get("/",PostsController.getPosts);

router.get("/:postId",PostsController.getPost,requiresAuth);

router.post("/",PostsController.createPost,requiresAuth);
router.patch("/:postId",PostsController.updatePost);

 router.delete("/:postId", PostsController.deletePost,requiresAuth);

export default router;
