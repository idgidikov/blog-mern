# Blog MERN Setup Guide

## Getting Started

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/idgidikov/blog-mern.git

Navigate to the blog-mern directory.
Backend Setup
Navigate to the backend directory:

```
cd blog-mern/backend
```


install project dependencies:

```
npm install
```

Start the backend app in development mode:
```
npm start
```
After starting, you should see the message "Mongoose connected" and "Server running on port: 5000".

If you encounter any issues, open the .env file and replace MONGO_CONNECTION_STRING with a valid link from your profile.
if you don't have MongoDB ```https://www.mongodb.com/docs/manual/administration/install-community/```

Frontend Setup
Navigate to the front directory:

```
cd blog-mern/frontend
```


install project dependencies:

```
npm install
```

Start the front app in development mode:
```
npm start
```

add redux dev tools to your browser so you can monitor the global state of the application

Now, your Blog MERN app should be up and running in development mode. Happy coding!