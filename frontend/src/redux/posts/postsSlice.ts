import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import * as PostApi from '../../network/posts_api';
import { Post } from "../../models/post";

interface PostState {
  posts: Post[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
}

const initialState: PostState = {
  posts: [],
  status: 'idle',
  error: null,
};


export const fetchPosts = createAsyncThunk<Post[]>('posts/fetchPosts', async () => {
  const response = await PostApi.getPosts();
 
  return response;
});


export const createPostAction = createAsyncThunk<Post, { post: PostApi.PostInput, userId: string }>(
  'posts/createPost',
  async ({ post }, { dispatch }) => {
    const response = await PostApi.createPost(post);
    dispatch(fetchPosts()); 
    return response;
  }
);


export const updatePostAction = createAsyncThunk<Post, { postId: string, post: Post, userId: string }>(
  'posts/updatePost',
  async ({ postId, post, userId }, { dispatch }) => {
    const response = await PostApi.updatePost(postId, post);
    dispatch(fetchPosts()); 
    return response;
  }
);

export const deletePost = createAsyncThunk<void, { postId: string, userId: string }>(
  'posts/deletePost',
  async ({ postId,}, { dispatch }) => {
    await PostApi.deletePost(postId);
    dispatch(fetchPosts()); 
  }
);

const postSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchPosts.fulfilled, (state, action: PayloadAction<Post[]>) => {
        state.status = 'succeeded';
        state.posts = action.payload;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(createPostAction.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(createPostAction.fulfilled, (state, action: PayloadAction<Post>) => {
        state.status = 'succeeded';
        state.posts.push(action.payload);
      })
      .addCase(createPostAction.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(updatePostAction.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(updatePostAction.fulfilled, (state, action: PayloadAction<Post>) => {
        state.status = 'succeeded';
      
        const updatedPostIndex = state.posts.findIndex(post => post.id === action.payload.id);
        if (updatedPostIndex !== -1) {
          state.posts[updatedPostIndex] = action.payload;
        }
      })
      .addCase(updatePostAction.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(deletePost.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(deletePost.fulfilled, (state, action) => {
        state.status = 'succeeded';
        
        state.posts = state.posts.filter(post => post.id !== action.meta.arg.postId);
      })
      .addCase(deletePost.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      });
  },
});

export default postSlice.reducer;
