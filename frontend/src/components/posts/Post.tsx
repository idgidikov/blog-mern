
import {Post as PostModel} from "../../models/post";
import {MdDelete} from "react-icons/md"
interface PostProps {
    post : PostModel
    onDeletePostClicked: (post: PostModel) => void
}
const Post = ({post, onDeletePostClicked} : PostProps)=>{
    const {id,title, text , createdAt} = post
    return(
    <div className="card w-96 bg-base-100 shadow-xl">
  <div className="card-body">
    <div>
    <h2 className="card-title">{title}</h2>
    <MdDelete onClick={(e)=>{
      onDeletePostClicked(post);
      e.stopPropagation();
    }}/>
    </div>
  
    
   
    <p>{text}</p>

    <div className="card-actions justify-end">
      <button className="btn btn-primary">Post detail</button>
    </div>
  </div>
</div>
)
}

export default Post;