import React from 'react';
import { useAppSelector } from '../../hooks/storeHook';
import { useParams} from 'react-router-dom';
import UserCard from '../../components/users/UserCard'; 
import PostCard from '../../components/posts/PostCard'; 

interface RouteParams {
  userId: string;
}

function UserDetails() {
  const { userId } = useParams();
  
  const users = useAppSelector((state) => state.users.users);
  const posts = useAppSelector((state) => state.posts.posts);
  const user = users?.find((u) => u.id === userId);

  if (!user) {
    return <div>User not found</div>;
  }
  const userPosts = posts.filter((p) => p.userId === userId);

  return (
    <div>
      <h1>User Details</h1>
      <UserCard user={user} />

      <h2>User's Posts</h2>
      {userPosts.length > 0 ? (
        userPosts.map((post) => <PostCard key={post.id} post={post} />)
      ) : (<>
        
        <p>No posts from this user.</p>
        </>
      )}
    </div>
  );
}

export default UserDetails;
