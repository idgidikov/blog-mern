import React, { useState } from "react";
import { PostInput,  } from "../../network/posts_api";
import { useAppDispatch } from "../../hooks/storeHook";
import { useForm } from "react-hook-form";
import { createPostAction } from "../../redux/posts/postsSlice";
import { useNavigate } from "react-router-dom";

function AddPost() {
  


  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
    reset,
  } = useForm<PostInput>();
  const dispatch = useAppDispatch();
  const navigate = useNavigate()

  const onSubmit = async (data: PostInput) => {
    try {
      await dispatch(createPostAction({ post: data, userId: "yourUserId" }));

      reset();
      navigate("/posts")
      
    } catch (error) {
      console.error("Create post failed:", error);
    }
  };

  return (
    <>
    <h3 className="font-bold text-lg">Add Post</h3>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-4">
            <label htmlFor="title" className="block text-sm font-medium text-gray-700">
              Title
            </label>
            <input
              type="text"
              id="title"
              {...register("title", { required: "Title is required" })}
              className="input input-bordered input-secondary w-full"
            />
            {errors.title && <p className="text-red-500">{errors.title.message}</p>}
          </div>
          <div className="mb-4">
            <label htmlFor="text" className="block text-sm font-medium text-gray-700">
              Text
            </label>
            <textarea
              id="text"
              {...register("text")}
              className="input input-bordered input-secondary w-full h-32"
            />
          </div>
          <div className="flex justify-end">

            <button
              type="submit"
              className="btn btn-primary"
              disabled={isSubmitting}
            >
              Add Post
            </button>
          </div>
        </form>
    </>
  );
}

export default AddPost;