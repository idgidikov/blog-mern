import React from 'react';
import { useAppSelector } from '../../hooks/storeHook';
import PostCard from './PostCard';

function ListPosts() {
  const posts = useAppSelector((state) => state.posts.posts);

  return (
    <div>
      <h1>All posts</h1>
      <div className="flex flex-wrap justify-center">
        {posts.map((post) => (
          <PostCard key={post.id} post={post} />
        ))}
      </div>
    </div>
  );
}

export default ListPosts;