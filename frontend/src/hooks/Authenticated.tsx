import React from "react";
import { Navigate, useLocation } from "react-router-dom";
import { useAppSelector } from "./storeHook"; // Import your Redux selector for user authentication status
import { User } from "../models/user";

interface AuthenticatedProps {
  children: React.ReactNode;
}

const Authenticated: React.FC<AuthenticatedProps> = ({ children }) => {
  const location = useLocation();
  const user = useAppSelector((state) => state.users.user) as User; 
  
   

  if (!user || !user.username) {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return <>{children}</>;
};

export default Authenticated;
