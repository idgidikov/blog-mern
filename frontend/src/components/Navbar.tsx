import React from 'react';
import { Link, useNavigate } from 'react-router-dom';



import { useAppDispatch,useAppSelector } from '../hooks/storeHook';
import { logoutAction } from '../redux/users/usersSlice';
import { User } from '../models/user';
import { toast } from 'react-toastify';

function Navbar() {
  const user = useAppSelector((state) => state.users.user) as User;
  const dispatch = useAppDispatch();
const navigate = useNavigate();


  const handleLogout = () => {
    
    dispatch(logoutAction());
    toast.success("Logout successfully")
    
    navigate('/login');
  };

  return (
    <div className="navbar bg-base-100">
      <div className="flex-none">
        <button className="btn btn-square btn-ghost">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-5 h-5 stroke-current">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path>
          </svg>
        </button>
      </div>
      <div className="flex-none">
        <ul className="menu menu-horizontal p-0">
          <li className="menu-item mr-5">
            <Link to="/create-post">AddPost</Link>
          </li>
          <li className="menu-item mr-5">
            <Link to="/posts">Posts</Link>
          </li>
          <li className="menu-item mr-5">
            <Link to="/users">Users</Link>
          </li>
          {!user && (<>
          
          <li className="menu-item mr-5">
          <Link to="/login">Login</Link>
        </li>
        <li className="menu-item mr-5">
          <Link to="/signup">Signup</Link>
        </li>
        </>)
          }
          
        </ul>
        
      </div>
      <div className="flex-1">
        <a className="btn btn-ghost text-xl">{!!user && <h1>{user?.username} </h1>}</a>
      </div>
      
      <br />
      {user?.username ? (
        <div className="flex-none">
          <button onClick={handleLogout} className="btn btn-square btn-ghost">
            Logout
          </button>
        </div>
      ) : null}
    </div>
  );
}

export default Navbar;
