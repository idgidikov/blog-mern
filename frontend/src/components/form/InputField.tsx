interface InputFieldProps {
  label: string;
  name: string;
  placeholder: string;
  type?: string;
  register: any;
  error: any;
}

const InputField: React.FC<InputFieldProps> = ({
  label,
  name,
  placeholder,
  type = "text",
  register,
  error,
}) => (
  <>
    <input
      type={type}
      placeholder={label}
      {...register(name, { required: `${label} is required` })}
      className="input input-bordered input-secondary w-full max-w-xs mb-2"
    />
    {error && <p className="text-red-500">{error.message}</p>}
  </>
);

export default InputField
