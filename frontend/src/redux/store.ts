

import { combineReducers, configureStore } from '@reduxjs/toolkit';
import userReducer from './users/usersSlice'
import postReducer from './posts/postsSlice'

const rootReducer = combineReducers({
  users: userReducer,
  posts: postReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;