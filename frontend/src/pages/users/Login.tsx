import React from "react";
import { LoginCredentials } from "../../network/posts_api";
import { useForm } from "react-hook-form";

import { useAppDispatch,useAppSelector } from "../../hooks/storeHook";
import { fetchLoggedInUser, login } from "../../redux/users/usersSlice";
import { Link, useNavigate } from "react-router-dom";
import { current } from "@reduxjs/toolkit";
import { User } from "../../models/user";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<LoginCredentials>();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const users = useAppSelector((state) => state.users);

  const onSubmit = async (data: LoginCredentials) => {
    try {
    
      const resultAction = await dispatch(login({ ...data }));

      if (login.rejected.match(resultAction)) {
        toast.error("You are not registered!");
        navigate("/signup");
      } else {
        toast.success("Login successful!");
        navigate('/');
      }
      
      
    
     
      
    } catch (error) {
      console.error("Login failed:", error);
    }
  };

  return (
    <>
      <div className="flex items-center justify-center h-screen">
        <form
          className="flex flex-col items-center justify-center"
          onSubmit={handleSubmit(onSubmit)}
        >
          <input
            type="text"
            placeholder="Username"
            {...register("username", { required: "Username is required" })}
            className="input input-bordered input-secondary w-full max-w-xs mb-2"
          />
          {errors.username && <p className="text-red-500">{errors.username.message}</p>}

          <input
            type="password"
            placeholder="Password"
            {...register("password", { required: "Password is required" })}
            className="input input-bordered input-secondary w-full max-w-xs mb-2"
          />
        
          {errors.password && <p className="text-red-500">{errors.password.message}</p>}

          <button
            className="btn btn-secondary  hover:bg-violet-500 bg-violet-600"
            type="submit"
            disabled={isSubmitting}
          >
            Login
          </button>
          <h1>Don't have an account ?</h1>
          <Link className=" btn btn-sm btn-outline btn-primary" to="/signup" >Signup</Link>
        </form>
      </div>
    </>
  );
}

export default Login;