import React from 'react';
import { useAppSelector } from '../../hooks/storeHook';
import PostCard from '../../components/posts/PostCard';
import UserCard from '../../components/users/UserCard';

function ListPosts() {
  const users = useAppSelector((state) => state.users.users);

  return (
    <div>
      <h1>All users</h1>
      <div className="flex flex-wrap justify-center">
        <ul>
        {users.map((user) => (
          <UserCard key={user.username} user={user} />
        ))}
        </ul>
      </div>
    </div>
  );
}

export default ListPosts;