import  {Request , Response ,RequestHandler, NextFunction }from "express";

import PostModel from "../models/posts";
import createHttpError from "http-errors";
import mongoose from "mongoose";
import { assertIsDefined } from "../util/assertIsDefined";
export const getPosts: RequestHandler = async (req : Request ,res : Response, next : NextFunction)=>{
    try{
        const posts = await PostModel.find().exec();
        const postsWithIds = posts.map(post => {
            const { _id, ...postData } = post.toObject();
            return { id: _id, ...postData };
        });
        
        
        res.status(200).json(postsWithIds);
    }catch(error){
       
        next(error);
        
    }
    
};



export const getPost: RequestHandler = async (req : Request ,res : Response, next : NextFunction)=>{
    const postId = req.params.postId;
    const authenticatedUserId = req.session.userId;

    try{
        assertIsDefined(authenticatedUserId);
        if(!mongoose.isValidObjectId(postId)){
            throw createHttpError(400, "Ivalid post id");
        }
        const post = await PostModel.findById(postId).exec();
        if(!post){
            throw createHttpError(404, "Post not found");
        }
        const { _id, ...postDetails } = post.toObject();
        res.status(200).json({ id: _id, ...postDetails });
    }catch(error){
       
        next(error);
        
    }
    
};
interface  CreatePostBody {
    title?: string,
    text?: string,
}

export const createPost: RequestHandler<unknown,unknown,CreatePostBody,unknown> = async (req   ,res, next) =>{
    const title = req.body.title;
    const text  = req.body.text;
    const authenticatedUserId = req.session.userId;
    
    try{
        assertIsDefined(authenticatedUserId);
        if(!title){
            throw createHttpError(400, "Post must have a title");
        }
        const newPost = await PostModel.create({
            userId: authenticatedUserId,
            title: title,
            text: text

        });
        const { _id, ...postDetails } = newPost;
        res.status(201).json({ id: _id, ...postDetails });
        
    }catch(error){
       
        next(error);
        
    }
    
    
};
interface UpdatePostParams{
    postId:string
}
interface UpdatePostBody {
    title?:string,
    text?: string,
}
export const updatePost: RequestHandler<UpdatePostParams,unknown,UpdatePostBody, unknown> = async(req,res,next)=> {
    const postId = req.params.postId;
    const newTitle = req.body.title;
    const newText = req.body.text;
    const authenticatedUserId = req.session.userId;
    try{
        assertIsDefined(authenticatedUserId);
        if(!mongoose.isValidObjectId(postId)){
            throw createHttpError(400, "Ivalid post id");
        }
        if(!newTitle){
            throw createHttpError(400, "Post must have a title");
        }
        const post = await PostModel.findById(postId).exec();

        if(!post){
            throw createHttpError(404, "Post not found");
        }
        post.title= newTitle
        post.text = newText
        const updatedPost = await post.save();
        const { _id, ...postDetails } = updatedPost;
        res.status(200).json({ id: _id, ...postDetails });

    }catch(error){
        next(error);
    }
}



export const deletePost : RequestHandler = async(req , res , next) => {
    const postId = req.params.postId;
    const authenticatedUserId = req.session.userId;
    try{
        assertIsDefined(authenticatedUserId);
        if(!mongoose.isValidObjectId(postId)){
            throw createHttpError(400, "Invalid post id");
        }

        const post = await PostModel.findById(postId).exec();
        console.log(post)
        if(!post){
            throw createHttpError(404, "Post not found");
        }
        await post.deleteOne();

        res.sendStatus(204);

    }catch(error){
        next(error);

    }
}
