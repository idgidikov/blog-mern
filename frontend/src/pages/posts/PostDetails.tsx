import React from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/storeHook';
import { useNavigate, useParams } from 'react-router-dom';
import PostCard from '../../components/posts/PostCard';
import { deletePost } from '../../redux/posts/postsSlice';
import { User } from '../../models/user';


function PostDetails() {
  const { postId } = useParams();
  const posts = useAppSelector((state) => state.posts.posts);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const post = posts.find((p) => p.id === postId);
  const loggedInUser = useAppSelector((state) => state.users.user as User);

  
  
  if (!post) {
    return <div>Post not found</div>;
  }
  const handleDeleteClick = async () => {
    const userId = post.userId
    if (userId && loggedInUser?.id && userId === loggedInUser?.id) {
    
      await dispatch(deletePost({ postId: post.id, userId }));
      navigate('/posts');
    }
  };

  return (
    <div>
      <h1>Post Details</h1>
      <div className="m-4">
      <div className="card w-96 bg-base-100 shadow-xl">
        <div className="card-body items-center text-center">
          <h2 className="card-title">{post.title}</h2>
          <p>{post.text}</p>
          <div className="card-actions">
            <button disabled={post.userId !== loggedInUser?.id} className="btn btn-primary" onClick={handleDeleteClick} >Delete</button>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}

export default PostDetails;
