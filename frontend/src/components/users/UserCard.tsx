import React from 'react';
import { User } from '../../models/user';
import { useNavigate,useMatch } from 'react-router-dom';

interface UserCardProps {
  user: User;
}

function UserCard({ user }: UserCardProps) {
  const navigate = useNavigate();

  const handleUserDetailsClick = () => {
    navigate(`/users/${user.id}`);
  };
  const isUserDetailsPage = useMatch(`/users/${user.id}`);

  return (
    <div className="m-4">
      <div className="card w-96 bg-base-100 shadow-xl">
        <div className="card-body items-center text-center">
          <h2 className="card-title">{user.username}</h2>
          <p>Email: {user.email}</p>
          <div className="card-actions">
            <button className="btn btn-primary" onClick={handleUserDetailsClick}
            disabled={isUserDetailsPage ? true : undefined} >
              User Details
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserCard;
