import React from "react";
import { SignUpCredentials } from "../../network/posts_api";
import { useForm } from "react-hook-form";
import { useAppDispatch } from "../../hooks/storeHook";
import { signUp, login } from '../../redux/users/usersSlice';
import InputField from "../../components/form/InputField";
import { Link,useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const SignUp: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<SignUpCredentials>();
  
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const onSubmit = async (data: SignUpCredentials) => {
    try {
      await dispatch(signUp({ ...data,}));
      toast.success("Signup successfully !")
      navigate('/');
    } catch (error) {
      
      console.error("Signup failed:", error);
    }
  };

  return (
    <div className="flex items-center justify-center h-screen">
      <form
        className="flex flex-col items-center justify-center"
        onSubmit={handleSubmit(onSubmit)}
      >
        <InputField
          label="Username"
          name="username"
          placeholder="Username is required"
          register={register}
          error={errors.username}
        />

        <InputField
          label="Email"
          name="email"
          placeholder="Email is required"
          register={register}
          error={errors.email}
        />

        <InputField
          label="Password"
          name="password"
          placeholder="Password is required"
          type="password"
          register={register}
          error={errors.password}
        />

        <button
          className="btn btn-primary hover:bg-violet-500 bg-violet-600"
          type="submit"
          disabled={isSubmitting}
        >
          Submit
        </button>
        <h1>Already have an account?</h1>
      <Link className=" btn btn-sm btn-outline btn-primary"  to="/login" >login</Link>
      </form>
      
    </div>
  );
};

export default SignUp;