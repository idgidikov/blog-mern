import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import * as PostApi from '../../network/posts_api';
import { User } from '../../models/user';


export const fetchUsers = createAsyncThunk<User[]>('users/fetchUsers', async () => {
  const response = await PostApi.getUsers();
  return response;
});

export const signUp = createAsyncThunk<User, { username: string; email: string; password: string }>(
  'users/signUp',
  async ({ username, email, password }, { dispatch ,rejectWithValue}) => {
    const response = await PostApi.signUp({ username, email, password });
    dispatch(fetchUsers()); 
    return response;
  }
);

export const login = createAsyncThunk<User, { username: string; password: string }>(
  'users/login',
  async ({ username, password }, { dispatch }) => {
    const response = await PostApi.login({ username, password });
    dispatch(fetchUsers()); 
    return response;
  }
);

export const fetchLoggedInUser = createAsyncThunk<User>('users/fetchLoggedInUser', async () => {
  const response = await PostApi.getLoggedInUser();
  return response;
});

export const logoutAction = createAsyncThunk<void>('users/logout', async (_, { dispatch }) => {
  
  await PostApi.logout(); 
  dispatch(fetchLoggedInUser()); 
});
interface UserState {
  users: User[];
  user: User | {};
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
}

const initialState: UserState = {
  users: [],
  user: {},
  status: 'idle',
  error: null,
};

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setCurrentUser: (state, action: PayloadAction<User | {}>) => {
      state.user = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchLoggedInUser.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchLoggedInUser.fulfilled, (state, action: PayloadAction<User>) => {
        state.status = 'succeeded';
        state.user = action.payload;
      })
      .addCase(fetchLoggedInUser.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(fetchUsers.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchUsers.fulfilled, (state, action: PayloadAction<User[]>) => {
        state.status = 'succeeded';
        state.users = action.payload;
      })
      .addCase(fetchUsers.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(signUp.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(signUp.fulfilled, (state, action: PayloadAction<User>) => {
        const { username, email, id } = action.payload;
        state.user = { username, email, id };


      })
      .addCase(signUp.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(login.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(login.fulfilled, (state, action: PayloadAction<User>) => {
        state.status = 'succeeded';
        const { username, email, id } = action.payload;
        state.user = { username, email, id };
       
      })
      .addCase(login.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      }).addCase(logoutAction.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(logoutAction.fulfilled, (state) => {
        state.status = 'idle';
        state.user = {};
      })
      .addCase(logoutAction.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      });
      
  },
});

export default userSlice.reducer;
