import React, { useEffect } from "react";
import { Route, Routes, Navigate, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "./hooks/storeHook";
import { fetchLoggedInUser, fetchUsers } from "./redux/users/usersSlice";
import { fetchPosts } from "./redux/posts/postsSlice";
import SignUp from "./pages/users/SignUp";
import Login from "./pages/users/Login";
import Home from "./pages/Home";
import Navbar from "./components/Navbar";
import AddPost from "./components/posts/AddPost";
import PostDetails from "./pages/posts/PostDetails";
import ListPosts from "./components/posts/ListPosts";
import ListUsers from "./pages/users/ListUsers";
import UserDetails from "./pages/users/UserDetails";
import Authenticated from "./hooks/Authenticated";
import { User } from "./models/user";
import { ToastContainer } from "react-toastify";
function App() {
  const dispatch = useAppDispatch();
  const users = useAppSelector((state) => state.users);
  const navigate = useNavigate();
  useEffect(() => {
    (async () => {
      await dispatch(fetchUsers());
      await dispatch(fetchPosts());

      await dispatch(fetchLoggedInUser());
    })();
  }, [dispatch]);

  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Authenticated><Home /></Authenticated>} />
        <Route path="/posts" element={<ListPosts />} />
        <Route path="/users" element={<ListUsers />} />
        <Route
          path="/posts/:postId"
          element={
            <Authenticated>
              <PostDetails />
            </Authenticated>
          }
        />
        <Route path="/users/:userId" element={<UserDetails />} />

        <Route
          path="/create-post"
          element={
            <Authenticated>
              <AddPost />
            </Authenticated>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
      </Routes>
      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </>
  );
}

export default App;
