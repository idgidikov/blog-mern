import React from 'react';
import { Post } from '../../models/post';
import { useNavigate } from 'react-router-dom';

interface PostCardProps {
  post: Post;
}

function PostCard({ post }: PostCardProps) {
    const navigate = useNavigate();

    const handlePostDetailsClick = () => {
    
      navigate(`/posts/${post.id}`);
    };
  return (
    <div className="m-4">
      <div className="card w-96 bg-base-100 shadow-xl">
        <div className="card-body items-center text-center">
          <h2 className="card-title">{post.title}</h2>
          <p>{post.text}</p>
          <div className="card-actions">
            <button className="btn btn-primary" onClick={handlePostDetailsClick} >Post Details</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PostCard